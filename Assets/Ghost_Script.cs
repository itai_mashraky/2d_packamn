using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Ghost_Script : MonoBehaviour
{
    [SerializeField] protected NavMeshAgent agent;
    [SerializeField] private Transform Target;
    protected Vector3 BasePosition;

    // Start is called before the first frame update
    void Start()
    {
        BasePosition = transform.position;
        agent = GetComponent<NavMeshAgent>();
        agent.updateRotation = false;
        agent.updateUpAxis = false;
        Target = GameObject.Find("Player").transform;
        agent.SetDestination(Target.position);
    }

    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(Target.position, agent.destination) >= 0.5f)
        {
            agent.SetDestination(Target.position);
        }
    }

    public virtual void resetPos()
    {
        gameObject.GetComponent<NavMeshAgent>().enabled = false;
        transform.position = BasePosition;
        gameObject.GetComponent<NavMeshAgent>().enabled = true;

    }

}
